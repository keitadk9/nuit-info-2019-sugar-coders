const API_URL = "http://51.83.252.16:4000/";

var datas = {
    date_created: new Date().toISOString(),
    date_modified: new Date().toISOString(),
    birthday: "",
    student_city: {},
    parent_city: {},
    bac_city: {},
    has_bac: 'true',
    bac_mention: '',
    kids_supported: 0,
    parents_familial_status: '',
    brothers_and_sisters_in_college: 1,
    brothers_and_sisters_supported_by_parents: 1,
    brothers_and_sisters_supported_by_parents_below_max_age: 1,
    is_tax_related_to_parents: true,
    family_income: '0',
    personal_income: 0,
    filling_status: 'not started',
    housing_situation: 'accommodated',
    student_status: 'student',
    study_abroad: false,
    training_type: 'study_period',
    country_will_study: 'KH',
    trip_duration: 6,
    degree: 'dut',
    degree_saso: false,
    school: {},
    has_handicapped_status: false,
    post_bac_year: 2,
    will_do_internship: true,
    internship_city: {},
    french_internship_duration: 2,
    student_bank: 'banque-postale',
    parent_bank: 'banque-postale',
    want_callback: false,
    day: '17',
    month: '10',
    year: '2000',
    has_kids_supported: false,
    has_income_current_year: false,
    first_month_personal_income: '',
    second_month_personal_income: '',
    third_month_personal_income: ''
};



$(document).ready(() => {





    $("#school").keyup(() => {
        var text = $("#school").val();
        // if(text.length != 0){
        //     const URL = API_URL + "schools?q="+text;

        //     fetch(URL)
        //     .then(response => response.json())
        //     .then((data) => {

        //     })

        // }
    })

    function getPlacesDatas(id, cb) {
        fetch(API_URL + "place?placeid=" + id)
            .then((response) => response.text())
            .then((text) => {
                cb(JSON.parse(text));
            })
    }

    $("#school").autocomplete({
        source: API_URL + "schools",
        minLength: 2,
        select: function (event, ui) {
            datas.school = ui.tmp;
        }
    });

    $("#parent_city").autocomplete({
        source: API_URL + "places",
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.id;
            getPlacesDatas(id, (place) => {
                datas.parent_city = place;
            })
        }
    })

    $("#student_city").autocomplete({
        source: API_URL + "places",
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.id;
            getPlacesDatas(id, (place) => {
                datas.student_city = place;
                datas.student_city.extra = [];
            })
        }
    })

    $("#bac_city").autocomplete({
        source: API_URL + "places",
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.id;
            getPlacesDatas(id, (place) => {
                datas.bac_city = place;
                datas.bac_city.extra = [];
            })
        }
    })

    $("#internship_city").autocomplete({
        source: API_URL + "places",
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.id;
            getPlacesDatas(id, (place) => {
                datas.internship_city = place;
                datas.internship_city.extra = [];
            })
        }
    })

    $('#will_do_internship_true').change(function () {
        $(".city-internship").removeClass("d-none")
        $(".duration-internship").removeClass("d-none")

    });

    $('#will_do_internship_false').change(function () {
        $(".city-internship").addClass("d-none")
        $(".duration-internship").addClass("d-none")
    });


    $("button[type='submit']").click((e) => {
        e.preventDefault();
        datas.birthday = new Date($("#month").val() + "/" + $("#day").val() + "/" + $("#year").val());
        datas.parents_familial_status = $('input[name=parents_familial_status]:checked').val()
        // datas.parent_city = $('#parent_city').val()
        datas.brothers_and_sisters_supported_by_parents = $('input[name=brothers_and_sisters_supported_by_parents]:checked').val()
        datas.brothers_and_sisters_in_college = $('input[name=brothers_and_sisters_in_college]:checked').val()
        datas.brothers_and_sisters_supported_by_parents_below_max_age = $('input[name=brothers_and_sisters_supported_by_parents_below_max_age]:checked').val()
        datas.has_kids_supported = $('input[name=has_kids_supported]:checked').val()
        datas.has_handicapped_status = $('input[name=has_handicapped_status]:checked').val()
        datas.has_licence_wish = $('input[name=has_licence_wish]:checked').val()
        datas.student_bank = $('#student_bank').val()
        datas.health_insurence = $('input[name=health_insurence]:checked').val()
        datas.student_status = $('input[name=student_status]:checked').val()
        datas.school = $('#school').val()
        // datas.student_city = $('#student_city').val()
        datas.has_bac = $('input[name=has_bac]:checked').val()
        datas.bac_mention = $('input[name=bac_mention]:checked').val()
        // datas.bac_city = $('#bac_city').val()
        datas.degree = $('input[name=degree]:checked').val() //Quel diplome
        datas.post_bac_year = $('#post_bac_year').val()//Bac + combien ?
        datas.degree_saso = $('input[name=degree_saso]:checked').val()//sanitaire et social
        datas.study_abroad = $('input[name=study_abroad]:checked').val()
        datas.will_do_internship = $('input[name=will_do_internship]:checked').val()
        // datas.internship_city = $('#internship_city').val()
        datas.french_internship_duration = $('#french_internship_duration').val()
        datas.housing_situation = $('input[name=housing_situation]:checked').val()
        datas.will_look_for_housing = $('input[name=will_look_for_housing]:checked').val()
        datas.has_income_current_year = $('input[name=has_income_current_year]:checked').val()
        datas.family_income = $('#family_income').val()
        datas.is_tax_related_to_parents = $('input[name=is_tax_related_to_parents]:checked').val()
        datas.want_callback = $('input[name=want_callback]:checked').val()

        console.log(datas);


        const dataSended = {
            datas
        }
        const options = {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataSended)
        }

        fetch(API_URL + "calculate", options)
            .then((response) => response.json())
            .then(data => {
                console.log(data)
            })

    })








})