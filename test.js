const { NlpManager } = require('node-nlp');
const express = require('express');
const app = express();
const helmet = require('helmet');
var bodyParser = require('body-parser');

app.use(helmet())
const manager = new NlpManager({ languages: ['fr'] });
// Adds the utterances and intents for the NLP
manager.addDocument('fr', 'goodbye for now', 'greetings.bye');
manager.addDocument('fr', 'bye bye take care', 'greetings.bye');
manager.addDocument('fr', 'okay see you later', 'greetings.bye');
manager.addDocument('fr', 'bye for now', 'greetings.bye');
manager.addDocument('fr', 'i must go', 'greetings.bye');
manager.addDocument('fr', 'hello', 'greetings.hello');
manager.addDocument('fr', 'hi', 'greetings.hello');
manager.addDocument('fr', 'howdy', 'greetings.hello');

// Train also the NLG
manager.addAnswer('fr', 'greetings.bye', 'Till next time');
manager.addAnswer('fr', 'greetings.bye', 'see you soon!');
manager.addAnswer('fr', 'greetings.hello', 'Hey there!');
manager.addAnswer('fr', 'greetings.hello', 'Greetings!');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/*(async() => {
    await manager.train();
    manager.save();
    const response = await manager.process('fr', 'hi');
    console.log(response);
})();*/

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
});

app.get('/*', async function (req, res) {
        await manager.train();
        manager.save();
        const response = await manager.process('fr', req.query.question);
        res.json(response);
});

app.listen(3000);