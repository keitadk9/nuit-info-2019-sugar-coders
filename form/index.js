const request = require('request')
var cheerio = require('cheerio')
const express = require('express')
const fetch = require('node-fetch');
const app = express()
const helmet = require('helmet')
var bodyParser = require('body-parser')
var _ = require('lodash')

const cheerioAdv = require('cheerio-advanced-selectors')
cheerio = cheerioAdv.wrap(cheerio)
app.use(helmet())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

/*app.get('/', function (req, res) {
    request('https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal', function (error, response, body) {
        var $ = cheerio.load(body);
        var tmp = $($("#bodyContent #accueil_2017_contenu .accueil_2017_cadre")[1]).find("ul > li");
        var t = [];

        for(var i = 0; i < tmp.length - 3; i++){
            var el = tmp[i];
            var text = $(el).text();
            t.push({
                date: text.split(":")[0].trim(),
                article: text.split(":")[1].trim(),
            });
        }

        res.json(t)
    });
})
*/

app.get('/places', function (req, res) {
    var search = req.query.term;

    if(search == undefined){
        res.json({
            error: true,
            msg: "Requete mal formée"
        });
    }else{
        fetch(
            "https://www.fibii.co/ajax/places/?q=" + search, {
            "credentials": "include",
            "headers": {
                "accept": "application/json, text/plain, */*",
                "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
                "content-type": "application/json;charset=UTF-8",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "Host": "www.fibii.co",
                "Origin": "https://www.fibii.co",
                "Cookie": "uniqUserId=da7787f971c37a4ec75eb848b85ffe28; _ga=GA1.2.638973206.1575573300; _gid=GA1.2.1042103911.1575573300; G_ENABLED_IDPS=google; _fbp=fb.1.1575573301027.701024830; wiz-auth-wizbii=%7B%22user%22%3A%22sdfsdfsdf-sdfsdfsdf%22%2C%22token%22%3A%222fc80a351aeef6f1826914468e5f8dcd0b159badd48032cefae2126875cccc69%22%7D; _hjid=3e67e2fe-41c5-46e1-8e89-32c17b770859; _delighted_fst=1575573454812:{}; _delighted_lst=1575573458461:{%22token%22:%224ToXYNOU8buHNro8cQhYYPVK%22}; crisp-client%2Fsession%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=session_96d9dd99-220f-4e85-a0f5-299bb2fa116c; _gat=1; crisp-client%2Fsocket%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=0"
            },
            "referrer": "https://www.fibii.co/helps/form",
            "referrerPolicy": "no-referrer-when-downgrade",
            "method": "GET",
            "mode": "cors"
        })
        .then((response) => response.text())
        .then((text) => {
            var text = JSON.parse(text);
            res.json(_.map(text, function (v) { return {
                id: v.place_id,
                label: v.name,
                value: v.name,
                tmp: v
            }}));
        })
    }
});

app.get('/place', function (req, res) {
    var search = req.query.placeid;

    if(search == undefined){
        res.json({
            error: true,
            msg: "Requete mal formée"
        });
    }else{
        fetch(
            "https://www.fibii.co/ajax/places/details?placeid=" + search, {
            "credentials": "include",
            "headers": {
                "accept": "application/json, text/plain, */*",
                "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
                "content-type": "application/json;charset=UTF-8",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "Host": "www.fibii.co",
                "Origin": "https://www.fibii.co",
                "Cookie": "uniqUserId=da7787f971c37a4ec75eb848b85ffe28; _ga=GA1.2.638973206.1575573300; _gid=GA1.2.1042103911.1575573300; G_ENABLED_IDPS=google; _fbp=fb.1.1575573301027.701024830; wiz-auth-wizbii=%7B%22user%22%3A%22sdfsdfsdf-sdfsdfsdf%22%2C%22token%22%3A%222fc80a351aeef6f1826914468e5f8dcd0b159badd48032cefae2126875cccc69%22%7D; _hjid=3e67e2fe-41c5-46e1-8e89-32c17b770859; _delighted_fst=1575573454812:{}; _delighted_lst=1575573458461:{%22token%22:%224ToXYNOU8buHNro8cQhYYPVK%22}; crisp-client%2Fsession%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=session_96d9dd99-220f-4e85-a0f5-299bb2fa116c; _gat=1; crisp-client%2Fsocket%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=0"
            },
            "referrer": "https://www.fibii.co/helps/form",
            "referrerPolicy": "no-referrer-when-downgrade",
            "method": "GET",
            "mode": "cors"
        })
        .then((response) => response.text())
        .then((text) => {
            var text = JSON.parse(text);
            res.json(text);
        })
    }
});

app.get('/schools', function (req, res) {
    var search = req.query.term;
    console.log(search)

    if(search == undefined){
        res.json({
            error: true,
            msg: "Requete mal formée"
        });
    }else{
        fetch(
            "https://www.fibii.co/ajax/schools/" + search, {
            "credentials": "include",
            "headers": {
                "accept": "application/json, text/plain, */*",
                "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
                "content-type": "application/json;charset=UTF-8",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "Host": "www.fibii.co",
                "Origin": "https://www.fibii.co",
                "Cookie": "uniqUserId=da7787f971c37a4ec75eb848b85ffe28; _ga=GA1.2.638973206.1575573300; _gid=GA1.2.1042103911.1575573300; G_ENABLED_IDPS=google; _fbp=fb.1.1575573301027.701024830; wiz-auth-wizbii=%7B%22user%22%3A%22sdfsdfsdf-sdfsdfsdf%22%2C%22token%22%3A%222fc80a351aeef6f1826914468e5f8dcd0b159badd48032cefae2126875cccc69%22%7D; _hjid=3e67e2fe-41c5-46e1-8e89-32c17b770859; _delighted_fst=1575573454812:{}; _delighted_lst=1575573458461:{%22token%22:%224ToXYNOU8buHNro8cQhYYPVK%22}; crisp-client%2Fsession%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=session_96d9dd99-220f-4e85-a0f5-299bb2fa116c; _gat=1; crisp-client%2Fsocket%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=0"
            },
            "referrer": "https://www.fibii.co/helps/form",
            "referrerPolicy": "no-referrer-when-downgrade",
            "method": "GET",
            "mode": "cors"
        })
        .then((response) => response.text())
        .then((text) => {
            var text = JSON.parse(text);
            // console.log(text);
            res.json(_.map(text, function (v) { return {
                id: v._id,
                label: v.university,
                value: v.university,
                tmp: v
            }}));
        })
    }
});

app.post('/calculate', function (req, res) {
    var datas = req.body.datas;
    // console.log(req.body);
    console.log(datas);


    if(datas == undefined){
        res.send("mauvaise requete");
    } else {
        fetch(
            "https://www.fibii.co/api/help/calculate", {
            "credentials": "include",
            "headers": {
                "accept": "application/json, text/plain, */*",
                "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
                "content-type": "application/json;charset=UTF-8",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "Host": "www.fibii.co",
                "Origin": "https://www.fibii.co",
                "Cookie": "uniqUserId=da7787f971c37a4ec75eb848b85ffe28; _ga=GA1.2.638973206.1575573300; _gid=GA1.2.1042103911.1575573300; G_ENABLED_IDPS=google; _fbp=fb.1.1575573301027.701024830; wiz-auth-wizbii=%7B%22user%22%3A%22sdfsdfsdf-sdfsdfsdf%22%2C%22token%22%3A%222fc80a351aeef6f1826914468e5f8dcd0b159badd48032cefae2126875cccc69%22%7D; _hjid=3e67e2fe-41c5-46e1-8e89-32c17b770859; _delighted_fst=1575573454812:{}; _delighted_lst=1575573458461:{%22token%22:%224ToXYNOU8buHNro8cQhYYPVK%22}; crisp-client%2Fsession%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=session_96d9dd99-220f-4e85-a0f5-299bb2fa116c; _gat=1; crisp-client%2Fsocket%2F898fd8bc-d609-4815-bc40-e5f9d14496ee=0"
            },
            "referrer": "https://www.fibii.co/helps/form",
            "referrerPolicy": "no-referrer-when-downgrade",
            "body": datas,
            "method": "POST",
            "mode": "cors"
        })
        .then((response) => response.text())
        .then((text) => {
            console.log(text)
            var text = JSON.parse(text);
            res.json(text);
        })
    }
});



app.listen(4000)


//https://www.fibii.co/ajax/places/?q=Paris
//https://www.fibii.co/ajax/places/details?placeid=af9599b85ad97dcead64bbb7bc500445
//https://www.fibii.co/ajax/schools/Iut
