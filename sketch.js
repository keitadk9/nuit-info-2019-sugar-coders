let classifier;
const modal = document.getElementById("myModal");
console.log(modal);
// Model URL
let imageModelURL = './model/model.json';

// Video
let video;
let flippedVideo;
// To store the classification
let label = "";

let pauseScore = 0;

let bool = false;



function playVideo() {

    // Create the video
    video = createCapture(VIDEO);
    video.size(320, 240);
    video.hide();

    flippedVideo = ml5.flipImage(video)
    // Start classifying
    classifyVideo();
}

// Load the model first
function preload() {
    classifier = ml5.imageClassifier(imageModelURL);
}

function setup() {
    const canvas = createCanvas(320, 260);
    canvas.parent('modalContent');



}

function draw() {

    if(!flippedVideo){
        return;
    }
    background(0);
    // Draw the video
    image(flippedVideo, 0, 0);

    // Draw the label
    fill(255);
    textSize(16);
    textAlign(CENTER);
    text(label, width / 2, height - 4);
}

function classifyVideo() {
    flippedVideo = ml5.flipImage(video)
    classifier.classify(flippedVideo, gotResult);
}



// When we get a result
function gotResult(error, results) {
    // If there is an error
    if (error) {
        console.error(error);
        return;
    }
    // The results are in an array ordered by confidence.
    // console.log(results[0]);
    label = results[0].label;
    confidence = results[0].confidence
    //console.log(results[0].confidence);
    if(label == "L-Left" || label == "L-Right" && confidence > 0.8 ){
        pauseScore++;

    }else{
        pauseScore = 0;
    }


    if(pauseScore > 20){
        if(!bool){
            var body = document.getElementById("rickroll");
            body.innerHTML = "<iframe width=\"100%\" height='450' src=\"https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1\" frameborder=\"0\" allowfullscreen></iframe>";
            bool = true;
        }
    }

    console.log(pauseScore);
    // Classifiy again!
    classifyVideo();
}

$(document).ready(()=>{
    $("#myModal").on("hide.bs.modal",(e)=>{
        var rick = document.getElementById('rickroll');
        rick.innerHTML = "";
        window.location.reload()
    })
});