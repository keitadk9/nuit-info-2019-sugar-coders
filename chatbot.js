const { NlpManager } = require('node-nlp');
const express = require('express');
const app = express();
const helmet = require('helmet');
const bodyParser = require('body-parser');
app.use(helmet());
const manager = new NlpManager({ languages: ['fr'] });

var fs = require('fs'),
    readline = require('readline');

var rd = readline.createInterface({
        input: fs.createReadStream('./chatbot.csv'),
        output: process.stdout,
        console: false
});

var bool = false;

rd.on('line', function(line) {
        console.log(line);
        var parts = line.split(",");

        if(parts[1] === ""){
               bool = true;
        }

        let str = [];
        for(let i = 0; i < parts.length - 1; ++i)
                str.push(parts[i]);

        str = str.join(",");
        console.log(parts[parts.length-1])
        if(bool){
                manager.addAnswer('fr', parts[parts.length - 1], str);
        }else{
                manager.addDocument('fr', str, parts[parts.length - 1]);
        }
        //manager.addDocument('fr', 'goodbye for now', 'greetings.bye');
});


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/*(async() => {
    await manager.train();
    manager.save();
    const response = await manager.process('fr', 'hi');
    console.log(response);
})();*/

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
});

app.get('/*', async function (req, res) {
        await manager.train();
        manager.save();
        const response = await manager.process('fr', req.query.question);
        await res.json(response);
});

app.listen(3000);