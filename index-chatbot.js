var msg = document.getElementById("msg");
msg.addEventListener("keydown", event => {
    if (event.code === "Enter")
        send();
});

function send() {
    const quest = document.querySelector("input[name='message']");
    let question = quest.value;
    if (question === "")
        return;

    if (question.toUpperCase() === ("université paris descartes".toUpperCase())) {
        showModal();
    }

    if(question.toUpperCase().includes("heure".toUpperCase())){
        const conv = document.getElementById("conv");
        conv.innerHTML += "<span class='userMsg msg'>" + question + "</span>";
        setTimeout(function() {
            conv.innerHTML += "<span class='botMsg msg'> Il est " + new Date().getHours() + "h" + new Date().getMinutes() + "</span>";
            new Audio("notif.mp3").play();
            conv.scrollBy(0, 1000000000);
        }, 750);
        const msg = document.getElementById("msg");
        msg.value = "";
        return;
    }




    const conv = document.getElementById("conv");
    conv.innerHTML += "<span class='userMsg msg'>" + question + "</span>";

    fetch("http://51.83.252.16:3000/?question=" + encodeURI(question))
        .then((response) => response.text())
        .then((text) => {
            setTimeout(function() {
                try{
                    let ans = JSON.parse(text).answers[0].answer;

                    conv.innerHTML += "<span class='botMsg msg'>" + ans + "</span>";
                    new Audio("notif.mp3").play();

                    conv.scrollBy(0, 1000000000);
                } catch (e) {
                    fetch("http://51.83.252.16:3000/?question=" + encodeURI("aaaaa"))
                        .then((response) => response.text())
                        .then((text) => {
                            setTimeout(function() {
                                let ans = JSON.parse(text).answers[0].answer;

                                conv.innerHTML += "<span class='botMsg msg'>" + ans + "</span>";
                                new Audio("notif.mp3").play();

                                conv.scrollBy(0, 1000000000);
                            }, 750);
                        });
                    conv.scrollBy(0, 1000000000);
                }
            }, 750);
        });
    conv.scrollBy(0, 1000000000);

    const msg = document.getElementById("msg");
    msg.value = "";
}

function show() {
    const chatbot = document.getElementById("chatbot");
    chatbot.style.display = "block";
    let opacity = 0;
    let timer = setInterval(function() {
        if (chatbot.style.opacity === "1") {
            document.getElementById("showChat").style.display = "none";
            clearInterval(timer);
        } else {
            opacity += 0.05;
            chatbot.style.opacity = opacity;
        }
    }, 20);
}

function hide() {
    const chatbot = document.getElementById("chatbot");
    let opacity = 1;
    let timer = setInterval(function() {
        if (parseFloat(chatbot.style.opacity) <= 0) {
            chatbot.style.display = "none";
            document.getElementById("showChat").style.display = "block";
            clearInterval(timer);
        } else {
            opacity -= 0.05;
            chatbot.style.opacity = opacity;
        }
    }, 20);
}